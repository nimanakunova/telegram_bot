import os
import telebot
import time
import schedule
import yaml

bot = telebot.TeleBot(os.environ['API_TOKEN'])


def send_message(record):
    first_usher = record['first_usher']
    second_usher = record['second_usher']
    bot.send_message(os.environ['CHAT_ID'],
                     f'Добрый день, братья и сестры,\n'
                     f'в это воскресенье служат домашки:\n '

                     f'1. Служение: {first_usher}\n '

                     f'2. Служение: {second_usher}\n '

                     f'Пожалуйста распределитесь по обязанностям:\n'
                     f'Перед служением :\n '
                     f'*Температура 1 человек\n'
                     f'*Брызгать антисептик 1 человек\n'
                     f'*Открывать двери 2 человека\n '
                     f'После служения:\n'
                     f'Провести дезинфекцию\n'
                     f'P.S.Все изменения и замены , пожалуйста заранее\n '
                     f'договоритесь сами между собой:\n'
                     f'Приходите пожалуйста за полчаса до служения\n'
                     f'Проверяйте, чтобы все были в масках\n')


def main():
    with open(os.environ['DATA'], 'r', encoding="utf-8") as file:
        records = yaml.safe_load(file)

        for record in records:
            send_message(record)
            time.sleep(604800)  #pause: 604800sec=1week


if __name__ == '__main__':
    try:
        schedule.every().tuesday.at("20:15").do(main)
        while True:
            schedule.run_pending()
    except KeyboardInterrupt:
        exit()
